let http = require('http')

let port = 4000

const server = http.createServer(function(request, response) {
	if(request.url == '/items' && request.method == 'GET') {
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Data retrieved from the database')
	} if(request.url == '/items' && request.method == 'POST') {
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Data sent to the database')
	}
	
}).listen(port);

console.log(`Server is now accessible at localhost: ${port}`);